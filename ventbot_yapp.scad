// This is for printing an enclosure for Ventbot
// https://hackaday.io/project/186808-ventbot-warm-side-cool-cool-side-warm
// v2023-01-13
//
// Template from:
//-----------------------------------------------------------------------
// Yet Another Parameterized Projectbox generator
// See https://mrwheel-docs.gitbook.io/yappgenerator_en/ for
// documentation and a download pointer.
//-----------------------------------------------------------------------

// These "local" variables have to come before the "include" statement.
// The "ki_" numbers are taken directly from the property sheets of
// things on the KiCad PCB editor (or calculated from other "ki_" items).
// It can be a little confusing because of the differences between the
// KiCad coordinate system and the orientations used varous places by
// YAPP.

// Typeface used for engraved text. OpenSCAD includes Liberation Mono,
// Liberation Sans, and Liberation Serif, which should work for
// everybody. Sans serif/mono thickish typefaces work best. I used
// Arial Black because its nice, thick characters work pretty well for
// 3D printing.  You probably have Arial Black already, but, if you
// don't, it's easy to find free downloads. You might have to slightly
// tweak the text positioning if you change the typeface. I did it by
// trial and error rather than calculations. OpenSCAD docs for text:
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Text

ventbot_typeface = "Arial Black:style=regular";

// Text is engraved into the base or lid surface (using an OpenSCAD
// difference operation). This value is the fraction of the base or
// lid thickness for the depth of that engraving. If you get close
// to or over 1.0, you might be surprised to find that little crumbs
// are left behind on the 3D printer bed. For example, the inside
// of the top of an "e".

ventbot_text_engrave_fraction = 0.75;

// Overall PCB coordinates

ki_pcb_start_x = 124.421;
ki_pcb_start_y =  45.062;
ki_pcb_end_x   = 195.421;
ki_pcb_end_y   = 144.062;
ki_pcb_size_x = ki_pcb_end_x - ki_pcb_start_x;
ki_pcb_size_y = ki_pcb_end_y - ki_pcb_start_y;

// The pins for the ESP32 module are rather long in comparison to the other
// solder spots, so allow extra clearance between the circuit board and
// the enclosure base.

ki_solder_clearance = 4;

// To allow for tolerance in the 3D printing of the enclosure, pretend
// the circuit board is 1 mm thicker than it actually is. This gives a
// little bit of "fudge" to make sure the enclosure lid and base can
// snap together properly.

ki_pcb_thickness = 1.6 + 1.0;

ki_component_offset_z = 0;

// Mounting holes, symmetrically spaced from all 4 corners

ki_hole_diameter = 4;
ki_hole_x        = 129.421;
ki_hole_y        =  50.062;
ki_hole_offset_x = ki_hole_x - ki_pcb_start_x;
ki_hole_offset_y = ki_hole_y - ki_pcb_start_y;
ki_poke_hole_offset = 6;

ki_fan_start_x = -1.77;
ki_fan_start_y = -3.00;
ki_fan_end_x = 9.39;
ki_fan_end_y = 3.00;
ki_fan_size_x = ki_fan_end_x - ki_fan_start_x;
ki_fan_size_y = ki_fan_end_y - ki_fan_start_y;
ki_fan_size_z = 7;

ki_fan1_ref_x = 135.421;
ki_fan1_ref_y =  48.662;
ki_fan1_x = ki_fan1_ref_x + ki_fan_start_x;
ki_fan1_y = ki_fan1_ref_y + ki_fan_start_y;
ki_fan1_offset_x = ki_fan1_x - ki_pcb_start_x;
ki_fan1_offset_y = ki_fan1_y - ki_pcb_start_y;
ki_fan1_cutout_x = ki_pcb_size_x - ki_fan1_offset_x - ki_fan_size_x;

ki_fan2_ref_x = 149.421;
ki_fan2_ref_y =  48.662;
ki_fan2_x = ki_fan2_ref_x + ki_fan_start_x;
ki_fan2_y = ki_fan2_ref_y + ki_fan_start_y;
ki_fan2_offset_x = ki_fan2_x - ki_pcb_start_x;
ki_fan2_offset_y = ki_fan2_y - ki_pcb_start_y;
ki_fan2_cutout_x = ki_pcb_size_x - ki_fan2_offset_x - ki_fan_size_x;

ki_fan3_ref_x = 163.421;
ki_fan3_ref_y =  48.662;
ki_fan3_x = ki_fan3_ref_x + ki_fan_start_x;
ki_fan3_y = ki_fan3_ref_y + ki_fan_start_y;
ki_fan3_offset_x = ki_fan3_x - ki_pcb_start_x;
ki_fan3_offset_y = ki_fan3_y - ki_pcb_start_y;
ki_fan3_cutout_x = ki_pcb_size_x - ki_fan3_offset_x - ki_fan_size_x;

ki_fan4_ref_x = 177.205;
ki_fan4_ref_y =  48.662;
ki_fan4_x = ki_fan4_ref_x + ki_fan_start_x;
ki_fan4_y = ki_fan4_ref_y + ki_fan_start_y;
ki_fan4_offset_x = ki_fan4_x - ki_pcb_start_x;
ki_fan4_offset_y = ki_fan4_y - ki_pcb_start_y;
ki_fan4_cutout_x = ki_pcb_size_x - ki_fan4_offset_x - ki_fan_size_x;

// I2C dupont pins
// This is on the back side, PCB orientation 180
//ki_j25_ref_x = 191.711;
ki_j25_ref_y =  92.612;
//ki_j25_start_x = -1.8;
ki_j25_start_y = -1.8;
//ki_j25_end_x = 1.8;
ki_j25_end_y = 9.4;
//ki_j25_x = ki_j25_ref_x - ki_j25_end_x;
ki_j25_y = ki_j25_ref_y - ki_j25_end_y;
ki_j25_offset_y = ki_j25_y - ki_pcb_start_y;
ki_j25_offset_z = ki_component_offset_z - 1;
ki_j25_width = ki_j25_end_y - ki_j25_start_y;
ki_j25_height = 9;

// I2C JST-PH
// This is on the back side, PCB orientation 90
//ki_j22_ref_x = 189.066;
ki_j22_ref_y = 106.378;
ki_j22_start_x = -2.45;
//ki_j22_start_y = -1.85;
ki_j22_end_x = 8.45;
//ki_j22_end_y = 6.75;
//ki_j22_x = ki_j22_ref_x + ki_j22_start_y;
ki_j22_y = ki_j22_ref_y - ki_j22_end_x;
ki_j22_offset_y = ki_j22_y - ki_pcb_start_y;
ki_j22_offset_z = ki_component_offset_z - 1;
ki_j22_width = ki_j22_end_x - ki_j22_start_x;
ki_j22_height = 8;

// I2C JST-SH
// This is on the back side, PCB orientation 90
//ki_j24_ref_x = ki_j22_ref_x;
ki_j24_ref_y = 115.316;
ki_j24_start_x = -3.9;
//ki_j24_start_y = -3.28;
ki_j24_end_x = 3.9;
//ki_j24_end_y = 3.28;
//ki_j24_x = ki_j24_ref_x + ki_j24_start_x;
ki_j24_y = ki_j24_ref_y - ki_j24_end_x;
ki_j24_offset_y = ki_j24_y - ki_pcb_start_y;
ki_j24_offset_z = ki_component_offset_z - 1;
ki_j24_width = ki_j24_end_x - ki_j24_start_x;
ki_j24_height = 6;

// Power
// This is on the back side, PCB orientation 90
//ki_pwr_ref_x = 182.627;
ki_pwr_ref_y = 129.812;
ki_pwr_start_x = -6;
//ki_pwr_start_y = -1.5;
ki_pwr_end_x = 6;
//ki_pwr_end_y = 12.5;
//ki_pwr_x = ki_pwr_ref_x + ki_pwr_start_y;
ki_pwr_y = ki_pwr_ref_y - ki_pwr_end_x;
ki_pwr_offset_y = ki_pwr_y - ki_pcb_start_y;
ki_pwr_offset_z = ki_component_offset_z - 2;
ki_pwr_width = ki_pwr_end_x - ki_pwr_start_x;
ki_pwr_height = 13;

// For ESP32 DevkitC v4, set offset_x to 22; for anything else, 17
devkitc_v4 = true;
ki_button_other_text = (devkitc_v4 ? "Test" : "");
ki_button_reset_text = (devkitc_v4 ? "Reset" : "");
ki_button_reset_offset_x = (devkitc_v4 ? 22 : 17);
ki_button_reset_offset_y = 19;
ki_button_other_offset_x = ki_button_reset_offset_x;
ki_button_other_offset_y = 34;
ki_button_reset_cutout_x = ki_button_reset_offset_x;
ki_button_reset_cutout_y = ki_pcb_size_y - ki_button_reset_offset_y;
ki_button_other_cutout_x = ki_button_other_offset_x;
ki_button_other_cutout_y = ki_pcb_size_y - ki_button_other_offset_y;
ki_button_reset_cutout_diameter = 5;
ki_button_other_cutout_diameter = 5;

grill_left_base = 16;
grill_slot_width = 2;
grill_slot_space = 3+grill_slot_width;
grill_long_start = 13;
grill_long_length = 46;
grill_short_start = ki_button_reset_offset_x+5;
grill_short_length = grill_long_length - (grill_short_start - grill_long_start);

include <YAPP_Box-1.6/library/YAPPgenerator_v16.scad>

printBaseShell      = true;
printLidShell       = true;

wallThickness       = 2.0;
basePlaneThickness  = 2.0;
lidPlaneThickness   = 2.0;

// These two heights were obtained by trial and error. The cutouts on
// the sides are either exactly at the seam where the base and lid
// join, or the seam splits the cutouts and shares it between base and
// lid. That allows you to print the whole enclosure without supports.

baseWallHeight      = 13;
lidWallHeight       = 13;

ridgeHeight         = 4;
ridgeSlack          = 0.2;
roundRadius         = 5.0;

standoffHeight      = ki_solder_clearance;
pinDiameter         = ki_hole_diameter - 0.4;  // avoid fitting too snugly
pinHoleSlack        = 0.3;
standoffDiameter    = pinDiameter + 2.5;

pcbLength           = ki_pcb_size_x;
pcbWidth            = ki_pcb_size_y;
pcbThickness        = ki_pcb_thickness;
                            
// This is padding between the PCB edges and the case walls. Left and right are the short sides.
// We need at least a little padding because the case has rounded internal corners, but
// the PCB has squared corners.

paddingFront        = 1;
paddingBack         = 1;
paddingRight        = 2;
paddingLeft         = 2;


//-- D E B U G -----------------//-> Default ---------
//showSideBySide      = false;     //-> true
//onLidGap            = 3;
//shiftLid            = 1;
//hideLidWalls        = false;    //-> false
//colorLid            = "yellow";   
//hideBaseWalls       = false;    //-> false
//colorBase           = "white";
//showOrientation     = true;
//showPCB             = true;
//showPCBmarkers      = true;
//showShellZero       = true;
//showCenterMarkers   = true;
//inspectX            = 0;        //-> 0=none (>0 from front, <0 from back)
//inspectY            = 0;        //-> 0=none (>0 from left, <0 from right)
//-- D E B U G ---------------------------------------


// These are the locations of the pins from the bottom of the case through
// the mounting holes on the PCB. You probably don't want to change these.
pcbStands = [
              [            ki_hole_offset_x,            ki_hole_offset_y, yappBoth, yappPin]
             ,[pcbLength - ki_hole_offset_x, pcbWidth - ki_hole_offset_y, yappBoth, yappPin]
             ,[            ki_hole_offset_y, pcbWidth - ki_hole_offset_y, yappBoth, yappPin]
             ,[pcbLength - ki_hole_offset_y,            ki_hole_offset_y, yappBoth, yappPin]
             ];

// What are these holes for? If you put the PCB onto the pins in the base and then
// want to remove the PCB, it can be tricky to get it off. You either damage the
// base while prying, or you break off some of the posts, or both. These holes let
// you push the PCB from underneath, hoping to avoid both of those problems most
// of the time.
cutoutsBase = [
                [            ki_hole_offset_x + ki_poke_hole_offset,            ki_hole_offset_y + ki_poke_hole_offset, ki_hole_diameter, 0, 0, yappCircle]
               ,[pcbLength - ki_hole_offset_x - ki_poke_hole_offset, pcbWidth - ki_hole_offset_y - ki_poke_hole_offset, ki_hole_diameter, 0, 0, yappCircle]
               ,[            ki_hole_offset_y + ki_poke_hole_offset, pcbWidth - ki_hole_offset_y - ki_poke_hole_offset, ki_hole_diameter, 0, 0, yappCircle]
               ,[pcbLength - ki_hole_offset_y - ki_poke_hole_offset,            ki_hole_offset_y + ki_poke_hole_offset, ki_hole_diameter, 0, 0, yappCircle]
             ];

cutoutsLid =  [
               // These circles let you poke the two on-board buttons through the case lid. The button
               // nearer the power jack is the reset button. The other button lets you temporarily
               // toggle the fans on or off.
                [ki_button_reset_cutout_x, ki_button_reset_cutout_y, ki_button_reset_cutout_diameter, 0, 0, yappCircle]
               ,[ki_button_other_cutout_x, ki_button_other_cutout_y, ki_button_other_cutout_diameter, 0, 0, yappCircle]

               // The cutoutsGrill has some peculiarities that I got tired of wrestling, so I made the
               // grill using a series of rectangular slices.
                ,[grill_long_start, grill_left_base+ 0*grill_slot_space, grill_slot_width, grill_long_length, 0, yappRectangle]
                ,[grill_long_start, grill_left_base+ 1*grill_slot_space, grill_slot_width, grill_long_length, 0, yappRectangle]
                ,[grill_long_start, grill_left_base+ 2*grill_slot_space, grill_slot_width, grill_long_length, 0, yappRectangle]
                ,[grill_long_start, grill_left_base+ 3*grill_slot_space, grill_slot_width, grill_long_length, 0, yappRectangle]
                ,[grill_long_start, grill_left_base+ 4*grill_slot_space, grill_slot_width, grill_long_length, 0, yappRectangle]
                ,[grill_long_start, grill_left_base+ 5*grill_slot_space, grill_slot_width, grill_long_length, 0, yappRectangle]
                ,[grill_long_start, grill_left_base+ 6*grill_slot_space, grill_slot_width, grill_long_length, 0, yappRectangle]
                ,[grill_long_start, grill_left_base+ 7*grill_slot_space, grill_slot_width, grill_long_length, 0, yappRectangle]
                ,[grill_long_start, grill_left_base+ 8*grill_slot_space, grill_slot_width, grill_long_length, 0, yappRectangle]

                // These grill slots are shorter to allow for a solid area in which the poke holes are placed.
                ,[grill_short_start, grill_left_base+ 9*grill_slot_space, grill_slot_width, grill_short_length, 0, yappRectangle]
                ,[grill_short_start, grill_left_base+10*grill_slot_space, grill_slot_width, grill_short_length, 0, yappRectangle]
                ,[grill_short_start, grill_left_base+11*grill_slot_space, grill_slot_width, grill_short_length, 0, yappRectangle]
                ,[grill_short_start, grill_left_base+12*grill_slot_space, grill_slot_width, grill_short_length, 0, yappRectangle]
                ,[grill_short_start, grill_left_base+13*grill_slot_space, grill_slot_width, grill_short_length, 0, yappRectangle]

               // These cut-outs in the lid are for vertical insertion of the fan connectors. They are a
               // little awkward to work with. If you use them, you will need to make some adjustments to 
               // "F1", etc, text.
               // The +/- 1 here is to allow a little slack for misalignment.
               //,[ki_fan1_cutout_x-1, ki_fan1_offset_y, ki_fan_size_y, ki_fan_size_x+1, 0, yappRectangle]  // Fan 1
               //,[ki_fan2_cutout_x-1, ki_fan2_offset_y, ki_fan_size_y, ki_fan_size_x+1, 0, yappRectangle]  // Fan 2
               //,[ki_fan3_cutout_x-1, ki_fan3_offset_y, ki_fan_size_y, ki_fan_size_x+1, 0, yappRectangle]  // Fan 3
               //,[ki_fan4_cutout_x-1, ki_fan4_offset_y, ki_fan_size_y, ki_fan_size_x+1, 0, yappRectangle]  // Fan 4

               // This makes one long cutout for all 4 fan connectors. Less fiddly. You can shorten it
               // if you are not going to use all 4 fans. Replace the "4" and/or "1" with the fans at the ends.
               //,
               // [ki_fan4_cutout_x-1, ki_fan4_offset_y, ki_fan_size_y, (ki_fan1_cutout_x - ki_fan4_cutout_x) + ki_fan_size_x + 1, 0, yappRectangle]  // Fan 1-4
              ];

cutoutsLeft =  [
                // [ki_fan1_cutout_x, ki_fan1_offset_y, ki_fan_size_x, ki_fan_size_y, 0, yappRectangle]  // Fan 1
                //,[ki_fan2_cutout_x, ki_fan2_offset_y, ki_fan_size_x, ki_fan_size_y, 0, yappRectangle]  // Fan 2
                //,[ki_fan3_cutout_x, ki_fan3_offset_y, ki_fan_size_x, ki_fan_size_y, 0, yappRectangle]  // Fan 3
                //,[ki_fan4_cutout_x, ki_fan4_offset_y, ki_fan_size_x, ki_fan_size_y, 0, yappRectangle]  // Fan 4

                // This makes one long cutout for all 4 fan connectors. Less fiddly. You can shorten it
                // if you are not going to use all 4 fans. Replace the "4" and/or "1" with the fans at the ends.
                // The +/- 1 here is to allow a little slack for misalignment.
                //,
                [ki_fan4_cutout_x-1, ki_fan1_offset_y, (ki_fan1_cutout_x - ki_fan4_cutout_x) + ki_fan_size_x + 1, ki_fan_size_y, 0, yappRectangle]  // Fan 1-4
              ];

// if you don't plan to use the Dupont, JST-SH, or JST-PH I2C connectors,
// you can comment out the lines for any unused cut-outs
cutoutsBack =   [
                    [ki_pwr_offset_y, ki_pwr_offset_z, ki_pwr_width,  ki_pwr_height, 0, yappRectangle] // power
                   ,[ki_j22_offset_y, ki_j22_offset_z, ki_j22_width,  ki_j22_height, 0, yappRectangle] // JST-PH
                   ,[ki_j24_offset_y, ki_j24_offset_z, ki_j24_width,  ki_j24_height, 0, yappRectangle] // JST-SH
                   ,[ki_j25_offset_y, ki_j25_offset_z, ki_j25_width,  ki_j25_height, 0, yappRectangle] // dupont
                ];

// This makes "ears" on the short sides of the base, which could be used for
// mounting the enclosure.
//baseMounts   = [
//                 [-5, 3.5, 10, 3, yappRight, yappCenter]
//                ,[-5, 3.5, 10, 3, yappLeft, yappCenter]
//               ];
               
// Little tabs for holding the two halves of the case together.
// You probably don't want to change this.
snapJoins   =     [
                     [ 2,  10, yappRight, yappSymmetric]
                    ,[20,  10, yappFront, yappSymmetric]
                    ,[20,  10, yappBack]
                ];
               
// These locations were all figured out by trial and error.
labelsPlane =  [
                  [62,  97, 180, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 8, "Ventbot" ]
                , [grill_short_start - 6,   71, 180, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 3, ki_button_other_text ]
                , [grill_short_start - 6,   86, 180, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 3, ki_button_reset_text ]

                // The dot is a reminder of where pin 1 goes for the fan connectors.
                , [21,   8, 270, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 4, "." ]
                , [34,   8, 270, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 4, "." ]
                , [47,   8, 270, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 4, "." ]
                , [60,   8, 270, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 4, "." ]

                , [16,  15, 270, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 4, "F4" ]
                , [29,  15, 270, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 4, "F3" ]
                , [42,  15, 270, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 4, "F2" ]
                , [55,  15, 270, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 4, "F1" ]

                , [ 6,  98, 270, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 4, "12v" ]
                , [ 6,  60, 270, lidPlaneThickness * ventbot_text_engrave_fraction, "lid",  ventbot_typeface, 4, "I2C: SCL.SDA.+.G" ]

                , [27,  97,  90, basePlaneThickness * ventbot_text_engrave_fraction, "base", ventbot_typeface, 5, "gitlab.com/"]
                , [34,  92,  90, basePlaneThickness * ventbot_text_engrave_fraction, "base", ventbot_typeface, 5, "wjcarpenter/ventbot"]
                , [49,  97,  90, basePlaneThickness * ventbot_text_engrave_fraction, "base", ventbot_typeface, 5, "hackaday.io/"]
                , [56,  92,  90, basePlaneThickness * ventbot_text_engrave_fraction, "base", ventbot_typeface, 5, "project/186808-ventbot"]

                  // if you are changing things and lose track of which side is which, you can uncomment these temporarily
                  //, [ 8,   5,   0, 1, "left",  ventbot_typeface, 7, "L" ]
                  //, [40,   5,   0, 1, "front", ventbot_typeface, 7, "F" ]
                  //, [5,    5,   0, 1, "back",  ventbot_typeface, 7, "B" ]
               ];


//---- This is where the magic happens ----
YAPPgenerate();
