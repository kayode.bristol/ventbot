globals:
  - id: IN_NEUTRAL_ZONE
    # This boolean indicates whether we are currently in the temperature
    # neutral zone. That is, above the low trigger and below the high trigger.
    type: 'bool'
    initial_value: 'true'

  - id: NEXT_DELAY
    # Much of the control code is implemented in C++ via ESPHome lambdas.
    # There is no way to safely/reliably do a native sleep inside that
    # code, so this global variable is used to communicate between the
    # C++ code and some ESPHome configuration actions. Value in seconds.
    type: float

  - id: HOLDING
    # A button press or service call has changed the fan speed and it
    # is being held that way for a while. While holding, the device
    # won't react to a reading from the temperature sensor. We start in
    # the HOLDING state at boot up so temperature readings don't mess
    # things up during auto-detect.
    type: 'bool'
    initial_value: 'true'

  - id: PREVIOUS_STATE
    # The reaction to a temperature reading is to determine the current
    # state (in or out of the neutral zone) and then see if that is
    # a state change that needs a reaction. This global keeps track of
    # the previous state for comparison.
    type: byte
    initial_value: ${STATE_NEUTRAL}

  - id: FAN_PRESENT
    # "present" means the fan controls its own speed; if it is controlled by
    # fan 1 for power or PWM, it doesn't count as present for this purpose.
    type: 'bool[${FAN_COUNT}]'

  - id: FAN_MINION
    # OTOH, if a fan in controlled by Fan 1, we call it a minion.
    type: 'bool[${FAN_COUNT}]'

esphome:
  name: ${name}
  project: {name: wjcarpenter.ventbot, version: "1.0"}
  on_boot: {priority: -100.0, then: [script.execute: S00_away_all_boats]}

esp32: {board: esp32dev}

wifi:
  use_address: ${name}.local
  ## CONFIG ##
  # If you configure a static address here, you might want to comment out
  # the "use_address" line just above.
  # manual_ip:
  #   static_ip: ${static_ip}
  #   gateway:   ${gateway}
  #   subnet:    ${subnet}
  #   dns1:      ${dns1}
  #   dns2:      ${dns2}
  ap:
    ssid: "${name} Config AP"
captive_portal:

logger: {level: '${log_level}'}
ota: {password: '${ota_password}'}
button:
  - platform: safe_mode
    id: i_safe_mode_button
    name: "${name} reboot-to-safe-mode button"

  - platform: shutdown
    name: "${name} shutdown button"

i2c:
  - id: i_i2c
    sda: ${I2C_SDA}
    scl: ${I2C_SCL}

switch:
  - {platform: restart, name: "${name} Reboot"}
  - {platform: safe_mode, id: i_safe_mode_switch, name: "${name} reboot-to-safe-mode switch"}

  - {platform: gpio, id: f1_power, name: '${name} F1 power', pin: '${F1_POWER}', restore_mode: ALWAYS_OFF, icon: mdi:power}
  - {platform: gpio, id: f2_power, name: '${name} F2 power', pin: '${F2_POWER}', restore_mode: ALWAYS_OFF, icon: mdi:power}
  - {platform: gpio, id: f3_power, name: '${name} F3 power', pin: '${F3_POWER}', restore_mode: ALWAYS_OFF, icon: mdi:power}
  - {platform: gpio, id: f4_power, name: '${name} F4 power', pin: '${F4_POWER}', restore_mode: ALWAYS_OFF, icon: mdi:power}

# For ESP32, channels 0-7 are high-speed channels, and 8-15 are low-speed channels.
# Low speed is plenty fast enough for our needs. If channels are not specified,
# ESPHome allocates them starting at 0 and going up one at a time. I found that
# there was some inconsistent behavior with the high-speed channels that I did
# not see with the low-speed channels, so I'm picking low-speed channel numbers
# explicitly. The odd behavior was sometimes not seeming to use the duty cycle
# that I wanted, which I observed with multiple fans spinning at obviously different
# speeds even though they all had the same duty cycle. I don't know the root cause.
# I don't think I ever saw it when testing with a single fan, so maybe it is
# some interaction across the channels.
output:
  - {platform: ledc, id: f1_pwm, pin: {number: '${F1_PWM}'}, frequency: 25000Hz, channel:  8}
  - {platform: ledc, id: f2_pwm, pin: {number: '${F2_PWM}'}, frequency: 25000Hz, channel:  9}
  - {platform: ledc, id: f3_pwm, pin: {number: '${F3_PWM}'}, frequency: 25000Hz, channel: 10}
  - {platform: ledc, id: f4_pwm, pin: {number: '${F4_PWM}'}, frequency: 25000Hz, channel: 11}

text_sensor:
  - platform: template
    name: "Actual sensor"
    id: i_actual_sensor
    update_interval: never

# The PWM setting is an output, so its value not reported to Home Assistant as an entity.
# We work around that by defining a template sensor and setting its value when we set
# the PWM duty cycle.
sensor:
  - {platform: template, name: "${name} F1 PWM", id: f1_pwm_pct, icon: mdi:square-wave, update_interval: never, unit_of_measurement: '%', accuracy_decimals: 0}
  - {platform: template, name: "${name} F2 PWM", id: f2_pwm_pct, icon: mdi:square-wave, update_interval: never, unit_of_measurement: '%', accuracy_decimals: 0}
  - {platform: template, name: "${name} F3 PWM", id: f3_pwm_pct, icon: mdi:square-wave, update_interval: never, unit_of_measurement: '%', accuracy_decimals: 0}
  - {platform: template, name: "${name} F4 PWM", id: f4_pwm_pct, icon: mdi:square-wave, update_interval: never, unit_of_measurement: '%', accuracy_decimals: 0}

  - platform: template
    name: "High temperature trigger"
    id: i_high_trigger
    update_interval: never

  - platform: template
    name: "Low temperature trigger"
    id: i_low_trigger
    update_interval: never

  - platform: pulse_counter
    id: f1_tach
    name: "${name} F1 tachometer"
    unit_of_measurement: rpm
    accuracy_decimals: 0
    icon: mdi:fan
    pin: {number: '${F1_TACH}', mode: {input: true, pullup: true}}
    update_interval: ${TACH_UPDATE_INTERVAL}
    filters: [multiply: 0.5]

  - platform: pulse_counter
    id: f2_tach
    name: "${name} F2 tachometer"
    unit_of_measurement: rpm
    accuracy_decimals: 0
    icon: mdi:fan
    pin: {number: '${F2_TACH}', mode: {input: true, pullup: true}}
    update_interval: ${TACH_UPDATE_INTERVAL}
    filters: [multiply: 0.5]

  - platform: pulse_counter
    id: f3_tach
    name: "${name} F3 tachometer"
    unit_of_measurement: rpm
    accuracy_decimals: 0
    icon: mdi:fan
    pin: {number: '${F3_TACH}', mode: {input: true, pullup: true}}
    update_interval: ${TACH_UPDATE_INTERVAL}
    filters: [multiply: 0.5]

  - platform: pulse_counter
    id: f4_tach
    name: "${name} F4 tachometer"
    unit_of_measurement: rpm
    accuracy_decimals: 0
    icon: mdi:fan
    pin: {number: '${F4_TACH}', mode: {input: true, pullup: true}}
    update_interval: ${TACH_UPDATE_INTERVAL}
    filters: [multiply: 0.5]

################################################################
# This section is a bunch of I2C temperature sensors. Some of
# them also sense relative humidity, atmospheric pressure, or
# something else. The something elses are not interesting for
# ventbot features.

# Ventbot only expects you to have one of these sensors actually
# hooked up. The ESPHome software will try to detect them all at
# boot-up time. You'll see errors in the log for the ones it can't
# find. That's OK because they'll be ignored after that.  The one that
# it does find will be used for actual temperature readings. If you do
# happen to wire up multiple sensors, things in ventbot should still
# work if all sensors are reporting reasonably close temperature
# readings.

# Of the sensors configured, I only tested bmp280 and bme280.  I'm
# assuming the other will work due to the robustness of the ESPHome
# software. However, you should probably read the documentation for
# any other sensor you try to use just in case there are quirks.
# I had a couple of sensors configured but discovered they did not
# report being in an error state, so you can't tell if they are
# working or not. I didn't care for that, so I dropped them back out.

# Most report temperatures in centigrade. If you use one that
# reports in Fahrenheit, adjust the value of SENSOR_IS_CENTIGRADE
# in your configuration.

# If you want to use an I2C sensor that is not listed here (but which
# is supported by ESPHome), there should be enough examples here to
# show you how to add it. Be sure to copy the on_value section
# verbatim. If your ESPHome complains about any of these, typically
# with a "Platform not found" error, it probably means that you are
# using an older ESPHome release than I am (ESPHome version
# v2023.5.5.4). You can either update your ESPHome software, or you can
# just find and remove the offending sensor.

# For every sensor listed here, there is a reference to it's
# component id later in the script S07_cancel_fan_hold. If you
# add or remove any sensors, you should add or remove the block
# from that script.
################################################################

  - platform: bmp280
    id: i_climate_sensor_bmp280
    address: ${ADDRESS_BMP280}
    update_interval: ${TEMP_UPDATE_INTERVAL}
    temperature:
      name: "${name} Temp"
      device_class: "temperature"
      on_value:
        then:
          - script.execute:
              id: 'S10a_on_temperature_reading'
              temperature_raw: !lambda 'return x;'
    pressure:
      name: "${name} AirPr"
      device_class: "pressure"

  - platform: bme280
    id: i_climate_sensor_bme280
    address: ${ADDRESS_BME280}
    update_interval: ${TEMP_UPDATE_INTERVAL}
    temperature:
      name: "${name} Temp"
      device_class: "temperature"
      on_value:
        then:
          - script.execute:
              id: 'S10a_on_temperature_reading'
              temperature_raw: !lambda 'return x;'
    pressure:
      name: "${name} AirPr"
      device_class: "pressure"
    humidity:
      name: "${name} RHumidity"
      device_class: "humidity"
      oversampling: 16x

# We don't try to use
#  bme680, bme680_bsec, bme085, bme3xx, dps310
# because the I2C addresses conflict with bmp280 or bme280.
# Otherwise, should work.
#  - platform: bme680
#  - platform: bme680_bsec
#  - platform: bme085
#  - platform: bme3xx

  - platform: sts3x
    id: i_climate_sensor_sts3x
    name: "${name} Temp"
    device_class: "temperature"
    on_value:
      then:
        - script.execute:
            id: 'S10a_on_temperature_reading'
            temperature_raw: !lambda 'return x;'

  # This is generally the same as DH12
  - platform: am2320
    id: i_climate_sensor_am2320
    update_interval: ${TEMP_UPDATE_INTERVAL}
    temperature:
      name: "${name} Temp"
      device_class: "temperature"
      on_value:
        then:
          - script.execute:
              id: 'S10a_on_temperature_reading'
              temperature_raw: !lambda 'return x;'
    humidity:
      name: "${name} RHumidity"
      device_class: "humidity"

  - platform: ens210
    id: i_climate_sensor_ens210
    update_interval: ${TEMP_UPDATE_INTERVAL}
    temperature:
      name: "${name} Temp"
      device_class: "temperature"
      on_value:
        then:
          - script.execute:
              id: 'S10a_on_temperature_reading'
              temperature_raw: !lambda 'return x;'
    humidity:
      name: "${name} RHumidity"
      device_class: "humidity"

  - platform: sht3xd
    id: i_climate_sensor_sht3xd
    update_interval: ${TEMP_UPDATE_INTERVAL}
    temperature:
      name: "${name} Temp"
      device_class: "temperature"
      on_value:
        then:
          - script.execute:
              id: 'S10a_on_temperature_reading'
              temperature_raw: !lambda 'return x;'
    humidity:
      name: "${name} RHumidity"
      device_class: "humidity"

  - platform: htu21d
    id: i_climate_sensor_htu21d
    update_interval: ${TEMP_UPDATE_INTERVAL}
    temperature:
      name: "${name} Temp"
      device_class: "temperature"
      on_value:
        then:
          - script.execute:
              id: 'S10a_on_temperature_reading'
              temperature_raw: !lambda 'return x;'
    humidity:
      name: "${name} RHumidity"
      device_class: "humidity"

  - platform: shtcx
    id: i_climate_sensor_shtcx
    update_interval: ${TEMP_UPDATE_INTERVAL}
    temperature:
      name: "${name} Temp"
      device_class: "temperature"
      on_value:
        then:
          - script.execute:
              id: 'S10a_on_temperature_reading'
              temperature_raw: !lambda 'return x;'
    humidity:
      name: "${name} RHumidity"
      device_class: "humidity"

  - platform: mpl3115a2
    id: i_climate_sensor_mpl3115a2
    update_interval: ${TEMP_UPDATE_INTERVAL}
    temperature:
      name: "${name} Temp"
      device_class: "temperature"
      on_value:
        then:
          - script.execute:
              id: 'S10a_on_temperature_reading'
              temperature_raw: !lambda 'return x;'
    pressure:
      name: "${name} AirPr"
      device_class: "pressure"

  - platform: mcp9808
    id: i_climate_sensor_mcp9808
    name: "${name} Temp"
    device_class: "temperature"
    on_value:
      then:
        - script.execute:
            id: 'S10a_on_temperature_reading'
            temperature_raw: !lambda 'return x;'

  - platform: scd4x
    id: i_climate_sensor_scd4x
    update_interval: ${TEMP_UPDATE_INTERVAL}
    temperature:
      name: "${name} Temp"
      device_class: "temperature"
      on_value:
        then:
          - script.execute:
              id: 'S10a_on_temperature_reading'
              temperature_raw: !lambda 'return x;'
    co2:
      name: "${name} CO2"
      device_class: "carbon dioxide"
    humidity:
      name: "${name} RHumidity"
      device_class: "humidity"

binary_sensor:
  - platform: gpio
    name: "${name} on-board button"
    id: i_button
    pin: {mode: input, number: '${BUTTON}', inverted: yes}
    on_click: {then: [script.execute: S11_button_press]}

api:
  # disables time-based reboot in case there is no local Home Assistant client
  reboot_timeout: 0s
  # These services can be called from Home Assistant for manual testing and temporary overrides.
  services:

    # Force pretending that the temperature is either in (true) or out (false) of the neutral zone.
    # Temperature sensor readings will be ignored until the duration expires or the hold is
    # canceled.
    - service: set_inout_neutral_zone_and_hold
      variables: {in_neutral_zone: bool, duration_seconds: int}
      then:
        - logger.log:
            tag: 'ventbot'
            level: INFO
            format: "service: set_inout_neutral_zone_and_hold, in_neutral_zone? %s, hold duration %d secs"
            args:
              - 'in_neutral_zone?"YES":"NO"'
              - duration_seconds
        - globals.set:
            id: IN_NEUTRAL_ZONE
            value: !lambda 'return in_neutral_zone;'
        - script.execute: S13_print_ramp_table
        - script.execute:
            id: S06_set_inout_neutral_zone_and_hold
            in_neutral_zone: !lambda 'return in_neutral_zone;'
            duration_seconds: !lambda 'return duration_seconds;'

    # This can be used to cancel a hold action before the duration has expired.
    - service: cancel_hold_inout_neutral_zone
      then:
        - logger.log:
            tag: 'ventbot'
            level: INFO
            format: "service: cancel_hold_inout_neutral_zone"
        - script.execute: S13_print_ramp_table
        - script.execute: S07_cancel_fan_hold

    # Moves the fan speeds one step UP (true) or DOWN (false) the ramp
    - service: crank_up_or_down_and_hold
      variables: {going_up: bool, duration_seconds: int}
      then:
        - logger.log:
            tag: 'ventbot'
            level: INFO
            format: "service: crank_up_or_down_and_hold, direction? %s, hold duration %d secs"
            args:
              - 'going_up?"UP":"DOWN"'
              - duration_seconds
        - script.execute: S13_print_ramp_table
        - script.execute:
            id: S08_crank_up_or_down_and_hold
            going_up: !lambda 'return going_up;'
            duration_seconds: !lambda 'return duration_seconds;'

script:
  - id: S00_away_all_boats
  # Things to do at boot time
    then:
      - sensor.template.publish:
          id: i_high_trigger
          state: ${HIGH_TRIGGER}
      - sensor.template.publish:
          id: i_low_trigger
          state: ${LOW_TRIGGER}

      # This delay is so that logged things don't get lost in start-up connectivity timing
      - delay: 4s
      - logger.log: {level: INFO, tag: 'ventbot', format: "script: S00_away_all_boats"}
      # We put the fans in HOLDING for a while so that we won't be
      # bothered with temperature reading. The hold gets cancelled
      # at the end of this script.
      - script.execute: {id: S05_hold_for_n_seconds, duration_seconds: 10000}

      - script.execute: S01_auto_detect_fan_presence
      - while:
          condition: 
            - script.is_running: S01_auto_detect_fan_presence
          then:
           - logger.log: {tag: 'ventbot', format: "Programmed delay during auto detection"}
           - delay: '5s'

      - if:
          condition:
            lambda: 'return ${DEMO_RAMPS_AT_BOOT};'
          then:
            - script.execute: S14_demo_ramp_up_down

      # no matter what else, cancel hold
      - script.execute: {id: S07_cancel_fan_hold}

  - id: S01_auto_detect_fan_presence
    then:
      - logger.log: {tag: 'ventbot', format: "script: S01_auto_detect_fan_presence"}

      # There are some intentional delays for fan spin-up, so we divided the
      # lambda script into phases and do the delay actions in between phases.
      # Fans identified via "fandex" use 0-based indexing.
      # Detect individual presence/controls
      - script.execute: {id: S09_auto_detect_lambda, phase: 1, fandex: 0}
      - delay: ${AUTO_DETECT_DELAY}
      - script.execute: {id: S09_auto_detect_lambda, phase: 2, fandex: 0}

      - script.execute: {id: S12_delay_until_fans234_stop}
      - while:
          condition:
            - script.is_running: S12_delay_until_fans234_stop
          then:
           - logger.log: {tag: 'ventbot', format: "Programmed delay"}
           - delay: '2s'

      - script.execute: {id: S09_auto_detect_lambda, phase: 1, fandex: 1}
      - delay: ${AUTO_DETECT_DELAY}
      - script.execute: {id: S09_auto_detect_lambda, phase: 2, fandex: 1}

      - script.execute: {id: S09_auto_detect_lambda, phase: 1, fandex: 2}
      - delay: ${AUTO_DETECT_DELAY}
      - script.execute: {id: S09_auto_detect_lambda, phase: 2, fandex: 2}

      - script.execute: {id: S09_auto_detect_lambda, phase: 1, fandex: 3}
      - delay: ${AUTO_DETECT_DELAY}
      - script.execute: {id: S09_auto_detect_lambda, phase: 2, fandex: 3}

      - script.execute: {id: S12_delay_until_fans234_stop}
      - while:
          condition:
            - script.is_running: S12_delay_until_fans234_stop
          then:
           - logger.log: {tag: 'ventbot', format: "Programmed delay"}
           - delay: '2s'

      # Detect the minions
      - script.execute: {id: S09_auto_detect_lambda, phase: 1, fandex: 0}
      - delay: ${AUTO_DETECT_DELAY}
      - script.execute: {id: S09_auto_detect_lambda, phase: 3, fandex: 1}
      - script.execute: {id: S09_auto_detect_lambda, phase: 3, fandex: 2}
      - script.execute: {id: S09_auto_detect_lambda, phase: 3, fandex: 3}

      - script.execute: {id: S04_direct_fan_control, fandex: 0, pwm: 0}

  - id: S02_ramp_up_down
  # Because of the need to do synchronous delays, and for clarity, we put the inidividual
  # ramp step activity into a separate script. It communicates back the amount of
  # loop delay via the NEXT_DELAY global variable. Using the separate ramp step script
  # also allows us to implement an externally-callable service to go up or down a step.
    mode: restart
    then:
      - globals.set: {id: NEXT_DELAY, value: '0'}
      - while:
          condition: {lambda: "return id(NEXT_DELAY) >= 0;"}
          then:
           - logger.log: {tag: 'ventbot', format: "DELAY %.2f sec", args: [id(NEXT_DELAY)]}
           - delay: !lambda 'return 1000 * id(NEXT_DELAY);'
           # if we are outside the neutral zone, spin up   the fans
           # if we are inside  the neutral zone, spin down the fans
           - script.execute:
               id: S03_ramp_up_down_lambda
               going_up: !lambda 'return id(IN_NEUTRAL_ZONE) ? false : true;'

  - id: S03_ramp_up_down_lambda
  # Implements going up or down a single step in the fan speed ramp table. Except when
  # at either end of the table, it always moves a single step, but a parameter tells
  # it whether to go up or down.
    parameters: {going_up: bool}
    then:
      - logger.log: {tag: 'ventbot', format: "script: S03_ramp_up_down_lambda, %s", args: ['going_up?"UP":"DOWN"']}
      - lambda: |-
          typedef struct {float duration_up; float duration_down; float pwm[${FAN_COUNT}];} RAMP_STEP;
          static RAMP_STEP steps[] = {${RAMP_TABLE} /*,*/ {-1}};
          // The extra array item above is just for silly C++ syntax reasons; otherwise, ignore it.
          // That's why we subtract 1 from the array size below.
          static short number_of_steps = (sizeof(steps) / sizeof(steps[0])) - 1;
          static bool first_time = true;
          if (first_time) {
            id(S13_print_ramp_table)->execute();
            first_time = false;
          }
          static int rampdex = 0;
          rampdex += (going_up ? 1 : -1);
          if (rampdex >= (number_of_steps)  ||  rampdex < 0) {
            if (going_up) {
              ESP_LOGD("ventbot", "^ ^ ^ ^ ^ ^ ^ ^ ^ ramp top");
              rampdex = number_of_steps - 1;
            } else {
              ESP_LOGD("ventbot", "v v v v v v v v v ramp bottom");
              rampdex = 0;
            }
            id(NEXT_DELAY) = -1;
          } else {
            RAMP_STEP *this_step = &(steps[rampdex]);
            float duration_after = (going_up ? this_step->duration_up : this_step->duration_down);
            ESP_LOGD("ventbot", "going %s, step %d, dur %.1f", going_up?"UP":"DOWN", rampdex, duration_after);
            for (int fandex=0; fandex<${FAN_COUNT}; ++fandex) {
              float pwm = this_step->pwm[fandex];
              if (! id(FAN_PRESENT)[fandex]) {
                ESP_LOGD("ventbot", "Fan %d not present; skipping", fandex+1);
                continue;
              }
              id(S04_direct_fan_control)->execute(fandex, pwm);
            }
            id(NEXT_DELAY) = duration_after;
          }

  - id: S04_direct_fan_control
    parameters: {fandex: int, pwm: float}
    then:
      - logger.log: {tag: 'ventbot', format: "S04_direct_fan_control, fandex %d (zero-based), pwm %f", args: [fandex, pwm]}
      - lambda: |
          if (pwm <= 1.0) {
            pwm *= 100.0;
          }
          ESP_LOGD("ventbot", "Setting Fan %d, PWM %.1f%%", fandex+1, pwm);
          // these assignments let me avoid knowing the actual types of the objects
          auto fn_pwm     = f1_pwm;
          auto fn_power   = f1_power;
          auto fn_pwm_pct = f1_pwm_pct;
          switch (fandex + 1) {
            case 1: fn_pwm = f1_pwm; fn_power = f1_power; fn_pwm_pct = f1_pwm_pct; break;
            case 2: fn_pwm = f2_pwm; fn_power = f2_power; fn_pwm_pct = f2_pwm_pct; break;
            case 3: fn_pwm = f3_pwm; fn_power = f3_power; fn_pwm_pct = f3_pwm_pct; break;
            case 4: fn_pwm = f4_pwm; fn_power = f4_power; fn_pwm_pct = f4_pwm_pct; break;
          }
          float apwm = 1.0 - (pwm/100.0);
          fn_pwm_pct->publish_state(pwm);
          ESP_LOGD("ventbot", "changing fan %d, %s (%.2f aka %.0f%%)", fandex+1, (pwm==0?"OFF":"ON "), apwm, pwm);
          if (pwm < 1) {
            fn_pwm->set_level(apwm);
            fn_pwm->turn_off();
            fn_power->turn_off();
          } else {
            fn_pwm->turn_on();  // must be on when setting level
            fn_pwm->set_level(apwm);
            fn_power->turn_on();
          }

  - id: S05_hold_for_n_seconds
    # The delay in here happens asynchronously, so it doesn't stop anything else from
    # running. Because this script is configured in restart mode, any activity that
    # places a new hold will replace this one with a new duration. If this one runs
    # to completion, it cancels itself.
    mode: restart
    parameters: {duration_seconds: int}
    then:
      - logger.log: {tag: 'ventbot', format: "script: S05_hold_for_n_seconds (%d)", args: [duration_seconds]}
      - globals.set: {id: HOLDING, value: 'true'}
      - globals.set: {id: PREVIOUS_STATE, value: '${STATE_HOLDING}'}
      - delay: !lambda 'return duration_seconds * 1000;'
      - script.execute: S07_cancel_fan_hold

  - id: S06_set_inout_neutral_zone_and_hold
    parameters: {in_neutral_zone: bool, duration_seconds: int}
    then:
      - logger.log: {tag: 'ventbot', format: "script: S06_set_inout_neutral_zone_and_hold (%s), %d seconds", args: ['in_neutral_zone?"TRUE":"FALSE"', duration_seconds]}
      - globals.set: {id: IN_NEUTRAL_ZONE, value: !lambda 'return in_neutral_zone;'}
      - script.execute:
          id: S05_hold_for_n_seconds
          duration_seconds: !lambda 'return duration_seconds;'
      - script.execute: S02_ramp_up_down

  - id: S07_cancel_fan_hold
    then:
      - logger.log: {tag: 'ventbot', format: "script: S07_cancel_fan_hold"}
      - script.stop: S05_hold_for_n_seconds
      - globals.set: {id: HOLDING, value: 'false'}

      # Check the temperature immediately to avoid waiting for the next interval to come around.
      # And also publish the non-failed sensor.
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_bme280)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'BME280'
            - component.update: i_climate_sensor_bme280
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_bmp280)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'BMP280'
            - component.update: i_climate_sensor_bmp280
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_am2320)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'AM2320'
            - component.update: i_climate_sensor_am2320
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_ens210)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'ENS210'
            - component.update: i_climate_sensor_ens210
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_htu21d)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'HTU21D'
            - component.update: i_climate_sensor_htu21d
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_bme280)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'BME280'
            - component.update: i_climate_sensor_bme280
      - component.update: i_climate_sensor_mcp9808
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_mpl3115a2)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'mpl3115a2'
            - component.update: i_climate_sensor_mpl3115a2
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_scd4x)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'SCD4X'
            - component.update: i_climate_sensor_scd4x
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_sht3xd)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'SHT3XD'
            - component.update: i_climate_sensor_sht3xd
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_shtcx)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'SHTCX'
            - component.update: i_climate_sensor_shtcx
      ##################################################################
      - if:
          condition:
            lambda: 'return !id(i_climate_sensor_sts3x)->is_failed();'
          then:
            - text_sensor.template.publish:
                id: i_actual_sensor
                state: 'STS3X'
            - component.update: i_climate_sensor_sts3x

  - id: S08_crank_up_or_down_and_hold
    parameters: {going_up: bool, duration_seconds: int}
    then:
      - logger.log: {tag: 'ventbot', format: "script: S08_crank_up_or_down_and_hold, %s %d seconds", args: ['going_up?"UP":"DOWN"', duration_seconds]}
      - script.stop: S02_ramp_up_down
      - script.execute:
          id: S05_hold_for_n_seconds
          duration_seconds: 300
      - script.execute:
          id: S03_ramp_up_down_lambda
          going_up: !lambda 'return going_up;'

  - id: S09_auto_detect_lambda
    parameters: {phase: int, fandex: int}
    then:
      - logger.log: {tag: 'ventbot', format: "script: S09_auto_detect_lambda, phase %d, fan %d", args: [phase, fandex+1]}
      - lambda: |-
          // this assignment lets me avoid knowing the actual types of the objects
          auto fn_tach    = f1_tach;
          switch (fandex + 1) {
            case 1: fn_tach = f1_tach; break;
            case 2: fn_tach = f2_tach; break;
            case 3: fn_tach = f3_tach; break;
            case 4: fn_tach = f4_tach; break;
          }

          if (phase == 1) {
            id(S04_direct_fan_control).execute(fandex, ${AUTO_DETECT_PWM});
          } else if (phase == 2) {
            static bool FORCED[] {${FORCE_PRESENT_FAN_1}, ${FORCE_PRESENT_FAN_2}, ${FORCE_PRESENT_FAN_3}, ${FORCE_PRESENT_FAN_4}};
            fn_tach->update();
            id(FAN_PRESENT)[fandex] = FORCED[fandex] || (fn_tach->state > ${AUTO_DETECT_THRESHOLD_RPM});
            id(S04_direct_fan_control).execute(fandex, 0);
            ESP_LOGI("ventbot", "Fan %d present? %s%s", fandex+1, (id(FAN_PRESENT)[fandex]) ? "YES" : "NO", FORCED[fandex]?" (forced)": "");
          } else if (phase == 3) {
            // this is checking for fans 2/3/4 controlled by fan 1 signals
            fn_tach->update();
            id(FAN_MINION)[fandex] = (fn_tach->state > ${AUTO_DETECT_THRESHOLD_RPM});
            id(S04_direct_fan_control).execute(fandex, 0);
            if (fandex != 0) {
              ESP_LOGI("ventbot", "Fan %d controlled by Fan 1? %s", fandex+1, (id(FAN_MINION)[fandex]) ? "YES" : "NO");
            }
          }

  - id: S10a_on_temperature_reading
    parameters: {temperature_raw: float}
    then:
      - logger.log: {tag: 'ventbot', format: "script: S10a_on_temperature_reading (%.1f raw)", args: [temperature_raw]}
      - if:
          condition:
            lambda: 'return id(HOLDING);'
          then:
            - logger.log: {tag: 'ventbot', format: "Holding; no reaction to temperature"}
          else:
            - script.execute:
                id: 'S10b_on_temperature_reading'
                temperature_raw: !lambda 'return temperature_raw;'

  - id: S10b_on_temperature_reading
    parameters: {temperature_raw: float}
    then:
      - logger.log: {tag: 'ventbot', format: "script: S10b_on_temperature_reading (%.1f raw)", args: [temperature_raw]}
      - lambda: |-
         float temperature_c = temperature_raw;
         float temperature_f = temperature_raw;
         if (${SENSOR_IS_CENTIGRADE}) {
           temperature_f = 32.0 + (9.0*temperature_c)/5.0;
         } else {
           temperature_c = (temperature_f - 32) * 5.0/9.0;
         }
         ESP_LOGD("ventbot", "        S10_on_temperature_reading (%.1f F)", temperature_f);
         ESP_LOGD("ventbot", "        S10_on_temperature_reading (%.1f C)", temperature_c);
         float temperature = (${TRIGGERS_ARE_CENTIGRADE}) ? temperature_c : temperature_f;
         byte current_state;
         if (temperature <= ${LOW_TRIGGER}) {
           current_state = ${STATE_COOL};
           id(IN_NEUTRAL_ZONE) = false;
         } else if (temperature >= ${HIGH_TRIGGER}) {
           current_state = ${STATE_WARM};
           id(IN_NEUTRAL_ZONE) = false;
         } else {
           current_state = ${STATE_NEUTRAL};
           id(IN_NEUTRAL_ZONE) = true;
         }
         if (current_state != id(PREVIOUS_STATE)) {
           id(PREVIOUS_STATE) = current_state;
           id(S02_ramp_up_down).execute();
         }

  - id: S11_button_press
    then:
      # You can use the on-board button for testing. It toggles the state of things
      # with respect to the temperature being in and out of the neutral zone,
      # regardless of the actual temperature facts.
      - logger.log: {tag: 'ventbot', level: INFO, format: "script: S11_button_press"}
      - lambda: 'id(IN_NEUTRAL_ZONE) = !id(IN_NEUTRAL_ZONE);'
      - script.execute:
          id: 'S06_set_inout_neutral_zone_and_hold'
          in_neutral_zone: !lambda 'return id(IN_NEUTRAL_ZONE);'
          duration_seconds: 300

  - id: S12_delay_until_fans234_stop
    then:
      # We need to delay enough for all the fans to spin down from the previous tests
      # to avoid a false readings.
      - logger.log: {tag: 'ventbot', level: INFO, format: "script: S12_delay_until_fans234_stop"}
      - component.update: f1_tach
      - component.update: f2_tach
      - component.update: f3_tach
      - component.update: f4_tach
      - while:
          condition:
            or:
              - sensor.in_range: {id: f1_tach, above: '${AUTO_DETECT_THRESHOLD_RPM}'}
              - sensor.in_range: {id: f2_tach, above: '${AUTO_DETECT_THRESHOLD_RPM}'}
              - sensor.in_range: {id: f3_tach, above: '${AUTO_DETECT_THRESHOLD_RPM}'}
              - sensor.in_range: {id: f4_tach, above: '${AUTO_DETECT_THRESHOLD_RPM}'}
          then:
           - logger.log: {tag: 'ventbot', format: "Waiting for fans 2/3/4 to spin down"}
           - delay: '1s'
           - component.update: f1_tach
           - component.update: f2_tach
           - component.update: f3_tach
           - component.update: f4_tach

  - id: S13_print_ramp_table
    then:
      - lambda: |-
          typedef struct {float duration_up; float duration_down; float pwm[${FAN_COUNT}];} RAMP_STEP;
          static RAMP_STEP steps[] = {${RAMP_TABLE} /*,*/ {-1}};
          // The extra array item above is just for silly C++ syntax reasons; otherwise, ignore it.
          // That's why we subtract 1 from the array size below.
          static short number_of_steps = (sizeof(steps) / sizeof(steps[0])) - 1;
          ESP_LOGI("ventbot", "Name: ${name}");
          ESP_LOGI("ventbot", "Temperature triggers: Low ${LOW_TRIGGER}, High ${HIGH_TRIGGER}");
          ESP_LOGI("ventbot", "There are %d ramp steps", number_of_steps);
          bool *fp = id(FAN_PRESENT);
          bool *fm = id(FAN_MINION);
          static char fc[${FAN_COUNT}][2] = {"1", "2", "3", "4"};
          ESP_LOGI("ventbot", "Fans:            Fan 1       2       3       4");
          ESP_LOGI("ventbot", "Fan control:         %s       %s       %s       %s", 
            fp[0] ? fc[0] :                   "-",
            fp[1] ? fc[1] : (fm[1] ? fc[0] : "-"),
            fp[2] ? fc[2] : (fm[2] ? fc[0] : "-"),
            fp[3] ? fc[3] : (fm[3] ? fc[0] : "-"));
          for (int ii=0; ii<number_of_steps; ++ii) {
            RAMP_STEP *s = &(steps[ii]);
            ESP_LOGI("ventbot", "  {%4.1f, %4.1f, {%6.2f, %6.2f, %6.2f, %6.2f}}, /*%2d*/",
              s->duration_up, s->duration_down, s->pwm[0], s->pwm[1], s->pwm[2], s->pwm[3], ii+1);
          }

  - id: S14_demo_ramp_up_down
    then:
      # Demonstrate ramp-up and then ramp-down
      - logger.log: {tag: 'ventbot', level: INFO, format: "script: S14_demo_ramp_up_down"}
      - script.execute: {id: S12_delay_until_fans234_stop}
      - while:
          condition: 
            - script.is_running: S12_delay_until_fans234_stop
          then:
           - logger.log: {tag: 'ventbot', format: "Programmed delay waiting for fan stops"}
           - delay: '2s'

      # ramp-up
      - script.execute:
         id: S06_set_inout_neutral_zone_and_hold
         in_neutral_zone: false
         duration_seconds: 10000
      - while:
          condition: 
            - script.is_running: S02_ramp_up_down
          then:
           - logger.log: {tag: 'ventbot', format: "Programmed delay during ramp-up"}
           - delay: '5s'

      # give fans a chance to fully spin up at the top of the ramp
      - delay: 5s

      # ramp-down
      - script.execute:
          id: S06_set_inout_neutral_zone_and_hold
          in_neutral_zone: true
          duration_seconds: 10000
      - while:
          condition: 
            - script.is_running: S02_ramp_up_down
          then:
           - logger.log: {tag: 'ventbot', format: "Programmed delay during ramp-down"}
           - delay: '5s'

      - script.execute: {id: S12_delay_until_fans234_stop}
      - while:
          condition:
            - script.is_running: S12_delay_until_fans234_stop
          then:
           - logger.log: {tag: 'ventbot', format: "Programmed delay"}
           - delay: '2s'

      - script.execute: {id: S07_cancel_fan_hold}
