# Ventbot

Do you have rooms in your house that are colder than other rooms in the winter? If you have air conditioning, are those same rooms warmer than other rooms in the summer? It's common for that to happen to rooms farthest from the blower on the furnace or air conditioner. You can buy booster fans that sit on top of or under registers to blow more air into those rooms.

With this project, you can make your own booster fans with behavior that is completely under your control.

The idea is to use commonly available and inexpensive PC case fans to do the work. A small circuit board and microprocessor provide the smarts. When the furnace or air conditioner is blowing warm or cool air into the room, the Ventbot automatically turns the fans on to provide a boost. When the furnace or air conditioner stops blowing, the Ventbot turns the fans off. Once you have it built and installed, you can just set it and forget it.

The repository contains all of the design files and software files for the project.
If you would like to read my porject notes as I worked by way though the evolution of it, you can find it here:
https://hackaday.io/project/186808-vent-bot-warm-side-cool-cool-side-warm

If want more details about how to build one, you can find a detailed Instructable here:
https://www.instructables.com/Ventbot-a-DIY-Home-HVAC-Vent-Booster-Warm-Side-Coo/

Finally, if you would like a to order the PCB that I used with a couple of clicks, you can do that here:
https://www.pcbway.com/project/shareproject/Ventbot_a_DIY_HVAC_Vent_Booster_Warm_Side_Cool_Cool_Side_Warm_76e090b7.html
