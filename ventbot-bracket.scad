// This is for printing a bracket for Ventbot.
// https://hackaday.io/project/186808-ventbot-warm-side-cool-cool-side-warm
// v2023-02-14

/* [Explanation and Terminology] */
// This bracket can either hang from tabs or stand on feet. The construction is the same for both, except that the tabs typically face outwards and the feet typically face inward for the best fit in the vent cavity. The choice is yours.
_intro = ""; // [-]
// The part of the bracket surrounding the fan itself is called the fan box, which has left and right sides, and a top and a bottom. The size of the fan box is given as a major size (similar to the fan diameter) and a minor size (the thickness of the body of the fan). The bracket's fan box holds two fans together, side by side, which each fan protruding halfway into the fan box. For each fan, two screws go through the top and two screws go though the bottom into the fan body.
_fan_box = ""; // [-]
// We refer to the overall bracket shape as a band (like a watch band). It has a thickness, typically a couple of millimeters, and a width, typically several millimetersa few centimeters. The width has to be wide enough to allow for the screw holes for two fans side by side.
_bracket = ""; // [-]
// The bracket is either held up on the sides of the vent opening by horizontal tabs, or stood up on the floor of the vent cavity on horizontal feet. The only real difference in construction is whether you orient those tabs/feet pointing inward or outward from the bracket.
_tabs_or_feet = ""; // [-]
//  Between the horizontal tabs/feet and the fan box are risers. You can choose where to attach the risers to the fan box. The fan box will be extended automatically if necessary to provide the attachment point.
_risers = ""; // [-]
// You can have a horizontal cross-brace between the two risers. This will probably give some additional lateral stability and resist movement due to vibration. You can put the cross-brace at any horizontal location since you might have to clear some other things that are situated between the tabs or feet.
_cross_braces = ""; // [-]

/* [Fan Box] */
// This is the size of the actual fan body, which will typically be slightly different (usually slightly larger) than the advertized size of the fan. It represents the distance between the left and right sides of the fan box. You don't have to adjust this if you adjust the band thickness.
fan_major_size = 95; // 0.1
// This is the thickness of the fan body, which will typically be slightly different than the advertised thickness of the fan. It represents the distance between the top and bottom of the fan box. You don't have to adjust this if you adjust the band thickness.
fan_minor_size = 26.5; // 0.1

/* [Risers] */

// This should be long enough so that the fans clear any protrusions below the register grating or on the vent cavity floor.
riser_length = 75; // 0.1
// Risers will be this far apart, which can be the same as, more than, or less than the fan major size. It's up to you to avoid attaching risers where the fan box screw holes reside.
riser_horizontal_distance = 105; // 0.1
// A cross-brace helps with lateral stability.
use_cross_brace = true;
// If you are using a cross-brace, it will be place horizontally at this vertical distance from the tabs/feet
cross_brace_vertical_distance = 60; // 0.1
// The horizontal length of the tabs or feet, measured from the vertical riser.
tab_length = 20; // 0.1
// Tabs can either face outward (for hanging) or face inward (for standing, in which case we call them feet). The reason you want feet to face inward is so that you can have the risers very close to the vent cavity walls, but the choice is yours.
tabs_face_outward = true;

/* [Screw Holes] */
// This is the center-to-center distance between the screw holes on any one side of the fan. Screw holes will be placed equidistant from the center of the long side (the major size) of the fan box top and bottom.
screwhole_major_distance = 82.3; // 0.1
// This is the center to center distance between the screw holes of two fans placed side by side. Screw holes will be placed equidistant from the center of the short side (the minor size) of the fan box top and bottom. You can go a little bigger to allow a gap betweeen two side by side fans, but you can go too small or things won't fit.
screwhole_minor_distance = 12; // 0.1
// This is the diameter of the screw hole. Case fan screws are a standard size (self-tapping, either 7/32 or M5), so there is usually no reason to change this.
screwhole_diameter = 4.8; // 0.05

/* [Band Dimensions] */
// The band just has to be thick enough to have strength to hold the suspended fans, including tolerating any vibration while they are running.
band_thickness = 2; // 0.1
// The band has to be wide enough so that the side-by-side screw holes can be placed. You don't want it to be too much wider since it will be partially blocking air flow from the fans.
band_width = 20; // 0.1

/* [Dimension Labels] */

// Select this if you want to show text labels on the various parts. That will help you to keep track of the dimensions you are entering elsewhere. The labels will not be part of the exported 3D model.
show_labels = true;
// When showing labels, what text size to use.
label_text_size = 3.5; // 0.1
// Font to use when displaying labels. You can find a font list in OpenSCAD via Help > Font List. If you don't specify a font, or if you specify an inappropriate font, you will get the OpenSCAD default font.
label_text_font = "";
// When showing labels for dimensions, what string to use for left-pointing arrow. Your font might not have the default Unicode characters.
left_arrow = "\u2190 ";
// When showing labels for dimensions, what string to use for right-pointing arrow. Your font might not have the default Unicode characters.
right_arrow = " \u2192";

/* [Hidden] */
// "fb" is fan box
// The inside of the fan box is the actual size of the fan, the outside is band thickness away.
// The outside of the fan box is at the X/Y origin. Everything starts at Z=0 and projects upwards.
fb_inner_left_x = band_thickness;
fb_inner_upper_y = band_thickness + fan_minor_size;
fb_inner_right_x = band_thickness + fan_major_size;
fb_inner_lower_y = band_thickness;
fb_center_x = ((2 * band_thickness) + fan_major_size) / 2;
fb_center_y = ((2 * band_thickness) + fan_minor_size) / 2;
// "rab" is riser attachment box, which comes into play if it's not the same as the fan box major size
rab_inner_left_x = band_thickness - (riser_horizontal_distance - fan_major_size)/2;
rab_inner_upper_y = fb_inner_upper_y;
rab_inner_right_x = rab_inner_left_x + riser_horizontal_distance;
rab_inner_lower_y = fb_inner_lower_y;
rab_center_x = fb_center_x;
rab_center_y = fb_center_y;
// "lr" is left riser, "rr" is right riser
lr_upper_y = rab_inner_upper_y + band_thickness + riser_length;
lr_right_x = rab_inner_left_x;
lr_left_x = lr_right_x - band_thickness;
rr_upper_y = lr_upper_y;
rr_left_x = rab_inner_right_x;
rr_right_x = rab_inner_right_x + band_thickness;
 
function make_label(label, number) = str(label, ": ", number, "mm", right_arrow);

module drawFlatFanBox() {
  difference() {
    square(size=[fan_major_size+(2*band_thickness), fan_minor_size+(2*band_thickness)], center=false);
    translate([fb_inner_left_x, fb_inner_lower_y])
      square(size=[fan_major_size, fan_minor_size], center=false);
  }
  %if (show_labels) {
    translate([fb_center_x, 3*band_thickness, 0])
      text(str(left_arrow, "fan major size: ", fan_major_size, "mm ", right_arrow), size=label_text_size, halign="center", valign="top", font=label_text_font);
    translate([fb_inner_right_x - 2*band_thickness, fb_inner_upper_y, 0])
    rotate(a=90)
      text(str(left_arrow, "fan minor size: ", fan_minor_size, "mm ", right_arrow), size=label_text_size, halign="right", valign="bottom", font=label_text_font);
  }
}

module drawFlatTabs() {
  if (tabs_face_outward) {
    translate([lr_left_x-tab_length, lr_upper_y-band_thickness, 0])
      square(size=[tab_length, band_thickness]);
    translate([rr_right_x, lr_upper_y-band_thickness, 0])
      square(size=[tab_length, band_thickness]);
    %if (show_labels) {
      translate([lr_left_x, lr_upper_y + band_thickness, 0])
        text(str(left_arrow, "tab length: ", tab_length, "mm ", right_arrow, "|"), size=label_text_size, halign="right", valign="bottom", font=label_text_font);
    }
  } else {
    translate([lr_right_x, lr_upper_y-band_thickness, 0])
      square(size=[tab_length, band_thickness]);
    translate([rr_left_x-tab_length, lr_upper_y-band_thickness, 0])
      square(size=[tab_length, band_thickness]);
    %if (show_labels) {
      translate([lr_right_x, lr_upper_y + band_thickness, 0])
        text(str("|", left_arrow, "tab length: ", tab_length, "mm ", right_arrow), size=label_text_size, halign="left", valign="bottom", font=label_text_font);
    }
  }
}

module drawFlatRisers() {
  if (riser_horizontal_distance > fan_major_size) {
    difference() {
      translate([-(riser_horizontal_distance - fan_major_size)/2, 0, 0])
        square(size=[riser_horizontal_distance+(2*band_thickness), fan_minor_size+(2*band_thickness)], center=false);
      translate([rab_inner_left_x, rab_inner_lower_y, 0])
        square(size=[riser_horizontal_distance, fan_minor_size], center=false);
    }
  }

  translate([rab_inner_left_x-band_thickness, rab_inner_upper_y+band_thickness, 0])
    square(size=[band_thickness, riser_length], center=false);
  translate([rab_inner_right_x, rab_inner_upper_y+band_thickness, 0])
    square(size=[band_thickness, riser_length], center=false);

  %if (show_labels) {
    translate([rab_inner_right_x + 3*band_thickness, rab_inner_upper_y + band_thickness, 0])
    rotate(a=90)
      text(str("|", left_arrow, "riser length: ", riser_length, "mm ", right_arrow), size=label_text_size, halign="left", valign="center", font=label_text_font);
    translate([rab_center_x, rab_inner_upper_y + riser_length - band_thickness, 0])
      text(str(left_arrow, "riser horizontal distance: ", riser_horizontal_distance, "mm ", right_arrow), size=label_text_size, halign="center", valign="top", font=label_text_font);
  }
}

module drawFlatCrossBrace() {
  if (use_cross_brace) {
    xb_lower_y = lr_upper_y-band_thickness-cross_brace_vertical_distance;
    translate([lr_right_x, xb_lower_y, 0])
      square(size = [riser_horizontal_distance, band_thickness]);
    %if (show_labels) {
      translate([rab_inner_right_x - band_thickness, xb_lower_y + band_thickness, 0])
        rotate(a=90)
        text(str("|", left_arrow, "cross brace vertical distance: ", cross_brace_vertical_distance, "mm ", right_arrow), size=label_text_size, halign="left", valign="bottom", font=label_text_font);
    }
  }
}

module drawFlat() {
  drawFlatFanBox();
  drawFlatRisers();
  drawFlatCrossBrace();
  drawFlatTabs();
}

module drawScrewHoles() {
  // We're going to draw cylinders that pass through both the top and bottom of the fan box.
  // We extend the cylinder to also cut holes in the cross-brace because otherwise it may
  // be difficult to reach the screws on the fan box top.
  dx = screwhole_major_distance / 2;
  dy = screwhole_minor_distance / 2;
  bc_x = band_width / 2;
  eps = 1;
  ht = fan_minor_size + 2*band_thickness + (riser_length - cross_brace_vertical_distance) + eps;
  places = [
            [fb_center_x+dx, 0, bc_x+dy],
            [fb_center_x-dx, 0, bc_x+dy],
            [fb_center_x+dx, 0, bc_x-dy],
            [fb_center_x-dx, 0, bc_x-dy]
            ];
  for (sh = places) {
    translate([sh.x, ht-eps/2, sh.z])
      rotate([90, 0, 0]) {
      cylinder(h=ht, r=screwhole_diameter/2, $fn=25);
    }
  }
}

module drawEverything() {
  difference() {
    linear_extrude(height=band_width, center=false)
      drawFlat();
    drawScrewHoles();
  }
}


drawEverything();
