# curious? https://hackaday.io/project/186808-ventbot-warm-side-cool-cool-side-warm
# Things that might be interesting for configuration fiddling are marked with
# ## CONFIG ##. Look for those, and leave other things alone unless you know what
# you are doing.

# To use this with multiple ventbots, make a copy of this file for each. Give
# each of your ventbots a unique name (just below). A useful convention would
# be to make the file names of the copies to be the same as the name. For example,
# if name is "ventbot-yellow", make a copy of this file called "ventbot-yellow.yaml".
# You'll then see each copy separately in the ESPHome dashboard. You will not
# usually need to make any changes to ventbot.yaml.inc, which is included by
# reference into each copy of this file. However, there are some configs below
# that would require edits to that file.

## CONFIG ##
# Some of these items use the "!secrets" mechanism. Find a description of
# how that works here: https://esphome.io/guides/faq.html#tips-for-using-esphome

substitutions:
  ## CONFIG ##
  # It is useful go give the device a unique suffix if you have more than one
  # or might have more than one in the future. Best to limit this overall name
  # to 32 characters or less.
  name: ventbot-indigo

  ## CONFIG ##
  # If you want to use a staic IP address, uncomment out these things and also
  # uncomment out the lines where they are used in ventbot.yaml.inc (for example,
  # look for ${static_ip} in that file.
  # static_ip: !secret ventbot_ip
  # gateway: !secret wifi_gateway
  # subnet:  !secret wifi_subnet
  # dns1:    !secret wifi_dns1
  # dns2:    !secret wifi_dns2

  ## CONFIG ##
  # Set a password to protect your device against rogue firmware updates.
  # I don't recommend disabling this, but if you do you must also comment
  # out the line where this is used later in ventbot.yaml.inc
  ota_password: !secret ota_password

  ## CONFIG ##
  # This table is used to control how the fans spin up (when the HVAC comes on) or
  # spin down (when the HVAC goes off). The speeds are changed in steps, with one
  # row for each step. When the fans are spinning up, the first number in the row
  # is how many seconds to pause after setting the fans to the values for that step.
  # The second number is similar, but it's seconds to pause when spinning down.
  # You might want those numbers to be the same, but it's not required. The
  # right-hand block of numbers in inner {braces} is one per fan. It's a PWM duty cycle
  # percentage, which means roughly the percentage of the fan's maximum speed. If
  # the value is 0, that fan is switched off. You can also use decimal fractions (0.4
  # for 40%). Any value 1 or smaller is interpreted as a decimal fraction, and
  # anything larger than 1 is interpeted as a percentage.
  #
  # There is a row of 0 values at the beginning of the table, which
  # means all the fans will be switched off after spin down is completed. That
  # matters for most fans since they keep spinning even with a PWM value of 0.
  # If for some reason you don't want to switch the fans completely off,
  # you can delete or change that row.
  #
  # The delay after the final ramp-up step is not meaningful since the fans
  # will continue to run indefinitely at the speeds given in the final step
  # (until something happens to tell them to spin down). Likewise, the delay
  # after the final ramp-down step is not meaningful.
  #
  # Fractional percentages are OK for all of these numbers, though that kind of
  # precision is not generally warranted. Be careful of the exact format for
  # punctuation or you will be greeted with incomprehensible gibberish when you
  # try to build it. Every block must end with a comma, including the last line.
  # Extra spaces are OK. For other esoteric syntax reasons that you probably
  # don't care about, maintain the left indentation amount (for YAML reasons)
  # and use /* */ commenting (for C++ and ESPHome reasons).
  #
  # You are not required to go all the way to 100% (maybe it's too loud for
  # you). You are also not required to go strictly increasing/decreasing.
  # If you just want to turn all the fans on immediately and completely, you
  # could just use this single line:
  #
  # {0, 0, {100, 100, 100, 100}},
  #
  # In my own configuration, I only go to 80% because the fans I use get
  # significantly louder above that. I start the fans up individually to
  # minimize the "audio shock" of hearing the start-up. I comment out the
  # start-up step for Fan 4 because I only have 3 fans. I hope these
  # examples will help you configure your own ramp up and ramp down steps
  # if you don't like mine.
  #
  # {up, down, { f1,  f2,  f3,  f4}},
  RAMP_TABLE: |-
    {0,  0,    {  0,   0,   0,   0}},
    {2,  1,    {  2,   0,   0,   0}},
    {2,  1,    {  2,   2,   0,   0}},
    {2,  1,    {  2,   2,   2,   0}},
    /*{2,  1,    {  2,   2,   2,   2}},*/
    {5,  5,    { 20,  20,  20,  20}},
    {5,  5,    { 40,  40,  .40,  40}},
    {5,  5,    { 60,  60,  60,  60}},
    {5,  5,    { 80,  80,  80,  80}},
    /*{0,  5,    {100, 100, 100, 100}},*/

  ## CONFIG ##
  # If the temperature is above the high trigger, the vent is blowing
  # heated air. If below the low trigger, the vent is blowing cooled
  # air. Boosting is desired for both cases.
  HIGH_TRIGGER: "85.1"
  LOW_TRIGGER:  "60.5"
  ## CONFIG ##
  # Most temp sensors read out in centigrade. You can specify the triggers in
  # either centrigrade or fahrenheit by setting these things appropriately.
  # The code will do conversion to the right scale.
  TRIGGERS_ARE_CENTIGRADE: 'false'
  SENSOR_IS_CENTIGRADE: 'true'

  ## CONFIG ##
  # These are how often the temperature and tachometer readings are taken.
  # The tach reading is mostly an FYI since it doesn't control anything.
  # If it's large compared to the delays in the ramp table, you might
  # not see true RPM readings for each step because it takes a little while
  # for the fan to physically change speed, and it takes the code a little
  # while to measure it.
  #
  # The temp reading can trigger the fans to turn on or off, so the
  # amount of delay here adds some lag time to how quickly the ventbot
  # will react to the HVAC coming on. A few seconds one way or the other
  # probably doesn't make much difference.
  TEMP_UPDATE_INTERVAL: '17s'
  TACH_UPDATE_INTERVAL: '12s'

  ## CONFIG ##
  # Consider this if you are using a BME280 or BMP280 and not getting
  # temperature readings from them.
  #
  # BME280 and BMP280 devices can have one of two I2C device addresses.
  # Usually it's BME280 at 0x77 and BMP280 at 0x76. However, they can
  # sometimes be the other way around, depending on the particular
  # breakout board for the sensor. In the startup code for the Ventbot,
  # there is an I2C bus scan, and the log will tell you what device
  # addresses it detected. Change the line below for the device that
  # you actually have so that the correct address is used. (You don't
  # normally have to change the other line, but you can if you want to.)
  ADDRESS_BME280:    '0x77'
  ADDRESS_BMP280:    '0x76'

  ## CONFIG ##  You probably don't need to change this. It's the verbose
  # level for the ESPHome console logging.
  log_level: DEBUG
  
  ## CONFIG ##  You probably don't need to change these.
  # Values used during fan auto-detection. The PWM value is a percentage
  # or decimal fraction (ie, 40% PWM is 0.4). The delay value lets the
  # fan spin up a bit before checking the tach value. The tach value
  # is compared against the threshold RPMs to decide if the fan is on.
  # For example, set the PWM to 40%, wait 3 seconds, and see if the
  # fan is spinning at 3 RPMs or more.
  AUTO_DETECT_PWM:           "40"
  AUTO_DETECT_DELAY:         '3s'
  AUTO_DETECT_THRESHOLD_RPM: '3'

  ## CONFIG ##
  # After fan auto-detection, Venbot goes through a full ramp-up, pauses for a
  # few seconds, and then goes through a ramp-down. The purpose of this is to
  # make it easy to listen for fan noise while you are tuning the ramp up and
  # ramp down table. If you are not doing that and prefer to save time, set this
  # to false.
  DEMO_RAMPS_AT_BOOT: 'true'

  ## CONFIG ##  You probably don't need to change these.
  # The code automatically detects which fans are actually present, but it
  # depends on the read-out of the tach signal from the fan. If that signal
  # is not present or is faulty or something, it wouldn't matter to the
  # functioning of the ventbot, but the auto-detection will fail for that
  # fan, and the rest of the code will completely ignore it. These configs give
  # you a way to claim the fan is present even if the auto-detection doesn't find
  # it. 
  FORCE_PRESENT_FAN_1: 'false'
  FORCE_PRESENT_FAN_2: 'false'
  FORCE_PRESENT_FAN_3: 'false'
  FORCE_PRESENT_FAN_4: 'false'

  ##################################################################
  # Substitutions below here are for my development convenience.
  # You shouldn't think they are configuration tunables. If you
  # change them, things are likely to break.
  ##################################################################

  FAN_COUNT: '4'

  F1_POWER: 'GPIO14'
  F1_TACH:  'GPIO25'
  F1_PWM:   'GPIO16'
  
  F2_POWER: 'GPIO13'
  F2_TACH:  'GPIO26'
  F2_PWM:   'GPIO17'
  
  F3_POWER: 'GPIO12'
  F3_TACH:  'GPIO27'
  F3_PWM:   'GPIO15'
  
  F4_POWER: 'GPIO23'
  F4_TACH:  'GPIO19'
  F4_PWM:   'GPIO18'
  
  BUTTON:   'GPIO0'

  I2C_SDA:  'GPIO21'
  I2C_SCL:  'GPIO22'

  STATE_COOL:    '1'
  STATE_NEUTRAL: '2'
  STATE_WARM:    '3'
  STATE_HOLDING: '4'

<<: !include { file: ventbot.yaml.inc}
